/* global module, require */

var middleware = require('browserify-middleware');
var brfs = require('brfs');

module.exports = function(options, imports, register) {
  'use strict';

  register(null, {
    browserify: middleware,
    brfs: brfs
  });
};
